# Extracción y transformación de series temporales de temperatura mínima

## Descripción del sistema
- Intel(R) Core(TM) i7-7700 CPU @ 3.60GHz
- 16 GB RAM
- 240 GB SSD
- Ubuntu 18.04.6 LTS

## Requisitos de software
- Docker Engine

## Cómo instalar Docker Engine
1. [Instalar desde el repositorio Docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
2. [Configurar permisos de usuario](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

## Versión de Docker Engine en el momento de la instalación
- Docker version 20.10.11, build dea9396

## Qué es una imagen Docker
Es importante tener en cuenta que una imagen Docker es un recurso que funciona únicamente sobre un núcleo Linux.  
Se debe evitar el uso de imágenes Docker para empaquetar aplicaciones de escritorio en entornos gráficos tipo GNOME, KDE o similar. Docker es una tecnología para la ejecución de servidores web, servidores de aplicación, servidores de base de datos, aplicaciones con interfaz en línea de comandos, secuencias de procesos y tareas programadas.  
Una imagen Docker es un binario que normalmente empaqueta un sistema operativo basado en Linux junto con aquellos servicios, librerías y utilidades de línea de comandos necesarias para ejecutar uno o más procesos de forma aislada. Se parece a una máquina virtual en cuanto que el sistema operativo de la imagen Docker se ejecuta en un espacio de memoria separado del sistema operativo anfitrión. Se diferencia de una máquina virtual en que el sistema operativo de la imagen Docker y el sistema operativo anfitrión comparten el núcleo Linux. En consecuencia, una imagen Docker cumple con los requisitos de reproducibilidad y separación de espacios de memoria de una máquina virtual, con la ventaja de que el tamaño del binario es menor. Al tratarse de un binario más pequeño, el tiempo de arranque y el consumo de memoria se reducen.

## Cómo compilar la imagen Docker
Una imagen se compila ejecutando una secuencia de instrucciones especificadas en un fichero de texto que, por convención, se denomina *Dockerfile*. Entre las [instrucciones admitidas](https://docs.docker.com/engine/reference/builder/) en un fichero *Dockerfile*, existen dos que son fundamentales: `FROM` y `RUN`. La instrucción `FROM` sirve para especificar la imagen base. La imagen base es una imagen preexistente sobre la que se añaden otros componentes para obtener una imagen derivada. Esos componentes se añaden mediante la instrucción `RUN`. Una imagen base puede ser cualquiera que se encuentre en el [repositorio público de imágenes denominado *Docker Hub*](https://hub.docker.com/) o cualquiera que haya sido compilada con anterioridad y se encuentre en el repositorio de la máquina local. En nuestro caso la imagen base es `debian:11.1-slim`, que se encuentra en [*Docker Hub*](https://hub.docker.com/_/debian?tab=description) y empaqueta el sistema operativo Debian 11.1. Sobre esta imagen base, compilamos una nueva imagen con los siguientes componentes adicionales que son necesarios para la extracción y transformación de series temporales de temperatura mínimas:  
  
- [Utilidades adicionales de BSD (bsdextrauils)](https://packages.debian.org/bullseye/bsdextrautils)
- [Geospatial Data Abstraction Library (GDAL)](http://nco.sourceforge.net/)
- [NetCDF Operators (NCO) toolkit](https://gdal.org/)
- [Servidor de base de datos PostgreSQL](https://www.postgresql.org/)
  
Para compilar la imagen ejecutamos el comando `docker build` en el mismo directorio en el que se encuentra el fichero *Dockerfile*, por ejemplo `$HOME/gitrepos/etl/tmin`:  
  
```
cd $HOME/gitrepos/etl/tmin
docker build -t quommit/tmin .
```  
  
Como resultado se crea una nueva imagen denominada `quommit/tmin` que queda registrada en el repositorio local de imágenes. Para listar todas las imágenes en local usamos el comando:  
  
```
docker image ls
REPOSITORY     TAG         IMAGE ID       CREATED          SIZE
quommit/tmin   latest      59f6e2a692e9   23 minutes ago   318MB
debian         11.1-slim   dfeb831cef5f   13 days ago      80.4MB
```  
  
El patrón convencional para denominar una imagen es *autor/nombre_imagen:version*. Si se omite la version, la imagen queda registrada por defecto con la versión *latest*. En nuestro caso, la denominación completa de la nueva imagen es `quommit/tmin:latest`, ya que se ha omitido el identificador de versión al ejecutar el comando `docker build`. En cuanto a la imagen base, una copia de la misma se almacena en el repositorio local para evitar descargas desde *Docker Hub* en posteriores recompilaciones.

## Qué es un contenedor Docker
Un contenedor Docker es una instancia ejecutable de una imagen Docker. Por tanto, en un punto en el tiempo, pueden coexistir varios contenedores de la misma imagen. Generalmente, un contenedor se encuentra en ejecución o detenido. Si está detenido, el contenedor no consume RAM ni ocupa tiempo de proceso. El ciclo de vida de un contenedor en el que se va a ejecutar un servicio (por ejemplo, un servidor PostgreSQL) suele ser más largo que el de un contenedor en el que se va a ejecutar una tarea programada o una utilidad de forma interactiva. En este último caso hablamos de contenedores efímeros ya que se detienen una vez que finaliza la tarea o el usuario termina la sesión en la línea de comandos. Para trabajar con series temporales de temperatura mínima vamos a usar un contenedor efímero que lee ficheros ASCII o NetCDF, extrae los datos del área de estudio y genera ficheros en un formato apto para su análisis posterior en otros entornos de cálculo o base de datos.

## Contenedores efímeros y PostgreSQL
Es importante tener en cuenta que el proceso de instalación, configuración e inicio de PostgreSQL que se ha escogido para nuestro caso de uso (contenedores efímeros para la extracción y transformación de series temporales de temperatura mínima) no es en absoluto aplicable para desplegar un servidor de base de datos en producción propiamente dicho. PostgreSQL es un servicio y, como tal, es un núcleo de proceso a partir del cual se crean y coordinan múltiples procesos (uno por cada conexión). Cuando se ejecuta PostgreSQL en un contenedor para dar servicio de base de datos en red, lo normal es que sea dedicado (no incorpora ningún software adicional salvo sus propias extensiones) y que se asignen permisos de acceso. Sin embargo, para el caso de un contenedor efímero, no existe ningún impedimento para incorporar PostgreSQL junto con otras librerías y programas en la misma imagen y usarlo como un componente más dentro de una cadena de procesos de extracción y transformación de datos. En este contexto, PostgreSQL sirve para realizar operaciones de álgebra relacional y volcar los datos de salida en el sistema de ficheros. Su cometido no es el almacenamiento persistente de datos, ni tampoco es necesario configurar permisos de acceso, dado que la base de datos sobre la que se opera es transitoria y queda desechada una vez se detiene el contenedor.

## Cómo se leen y se escriben datos desde un contenedor
Por defecto, un contenedor monta su propio sistema de ficheros, separado del sistema de ficheros del anfitrión. El inconveniente de esta arquitectura es que los datos que se escriben en el sistema de ficheros de un contenedor se borran cuando este se elimina. Si se necesita persistencia de datos, es decir, acceder al sistema de ficheros del anfitrión en modo lectura y escritura, debemos definir volúmenes. Un volumen es un directorio en el anfitrión que el sistema de ficheros del contenedor asume como propio. Los datos que el contenedor escribe en un volumen permanecen una vez que dicho contenedor se detiene y desaparece. Los volúmenes han de definirse en el momento en que un contenedor se ejecuta por primera vez. No es posible definir un volumen nuevo para un contenedor que está detenido y se va a reiniciar posteriormente. En nuestro caso, la nueva imagen compilada incluye un directorio `/data` que opera como directorio de trabajo por defecto y como punto de montaje del volumen donde se leen los ficheros de entrada y se escriben los de salida.

## Serie temporal de temperatura mínima de AEMET

### Descripción de los datos
AEMET proporciona la serie de temperaturas mínimas entre 1951 y 2020 para la Península y las Baleares en un archivo *tar.gz* que contiene:

- Un fichero ASCII de registros denominado 'fichero maestro' en el que cada registro indica la posición de un punto en una rejilla de 0.05 grados. Cada registro consta de cuatro campos de ancho fijo (identificador del punto, longitud, latitud y altitud). Este fichero maestro tiene 16156 registros ordenados por el campo identificador del punto. El identificador es un entero en el intervalo [1,16156].
- Un fichero ASCII de registros de temperatura mínima. Cada registro consta de 16157 campos de ancho fijo donde el primer campo indica la fecha de la estimación en formato básico ISO 8601 (*YYYYMMDD*), el segundo indica la temperatura mínima estimada para el primer punto de la rejilla, el tercero indica la temperatura mínima estimada para el segundo punto de la rejilla, y así sucesivamente. Hay in total de 25568 registros (los días que abarca la serie temporal) ordenados por el campo fecha de estimación.

El fichero `AEMET/data/DOWNLOAD` contiene la URL de la página de descargas de AEMET y la URL del fichero *tar.gz*.

### Cómo ejecutar un contenedor para procesar la serie de AEMET
Asumiendo que los datos de AEMET se almacenan en `$HOME/gitrepos/etl/tmin/AEMET/data`, la secuencia de instrucciones:

```
cd $HOME/gitrepos/etl/tmin/AEMET/data
docker run -it --rm --name aemet -e PROMPT=aemet -v $(pwd):/data quommit/tmin
```  
  
proporciona una nueva línea de comandos en el propio terminal con el *prompt* `root@aemet:/data$`. A partir de este punto, trabajamos de manera efectiva en un entorno virtualizado, al margen del sistema anfitrión, y es necesario tener en cuenta las siguientes claves para evitar confusiones:  
  
- Ha arrancado un nuevo contenedor, en nuestro caso una instancia ejecutable de la imagen `quommit/tmin:latest`.
- El contenedor tiene asignado el nombre `aemet` para distinguirlo de cualquier otro.
- Toda instrucción que se escriba en esta línea de comandos se ejecutará en el contenedor `aemet`.
- Las instrucciones se ejecutarán como superusuario (`root`) del contenedor, no como superusuario del sistema anfitrión.
- El directorio de trabajo actual del contenedor es `/data`.
- En el directorio `/data` del contenedor se ha montado un volumen que apunta a `$HOME/gitrepos/etl/tmin/AEMET/data` en el sistema de ficheros del anfitrión. Esto se consigue mediante la opción `-v $(pwd):/data` al ejecutar el comando `docker run`. Por tanto, `/data` es el directorio base en el que ha de escribirse cualquier fichero que sea necesario conservar una vez eliminado el contenedor.
- El contenedor es efímero: abandonar la línea de comandos mediante el comando `exit` provoca su detención y eliminación, y el regreso a la línea de comandos del sistema anfitrión.

### Cómo desempaquetar los datos de AEMET
La siguiente secuencia de comandos permite obtener el fichero maestro y el fichero de registros de temperatura mínima en el directorio `/data`:

```
root@aemet:/data$ #Listamos el contenido del directorio /data
root@aemet:/data$ ls -lh
total 541M
-rw-r--r-- 1 1000  1000  393 Dec 20 12:11 DOWNLOAD
-rw-rw-r-- 1 1000 1000 541M Nov 24 11:18 Serie_AEMET_v1_tmin_1951a2020_txt.tar.gz

root@aemet:/data$ #Extraemos los ficheros empaquetados en el archivo tar.gz
root@aemet:/data$ tar -xzf Serie_AEMET_v1_tmin_1951a2020_txt.tar.gz

root@aemet:/data$ #Listamos el contenido del directorio /data para comprobar que se han extraído
root@aemet:/data$ #el fichero maestro (maestro*.txt) y la serie de temperaturas (tmin*.geo)
root@aemet:/data$ ls -lh
total 3.7G
-rw-r--r-- 1 1000  1000  393 Dec 20 12:11 DOWNLOAD
-rwxr-xr-x 1 3405 50003 4.3K Jul  2 10:37 README.txt
-rw-r--r-- 1 1000  1000 541M Dec 15 10:22 Serie_AEMET_v1_tmin_1951a2020_txt.tar.gz
-rwxr-xr-x 1 3405 50003 632K Jul  2 10:37 maestro_red_hr_tmin.txt
-rw-r--r-- 1 3405 50003 3.1G Jul  2 10:32 tmin_red_SPAIN_1951-2020.geo
```

### Cómo crear un fichero CSV con los puntos de la rejilla de AEMET
El fichero maestro no tiene formato CSV. Está estructurado en registros de cuatro campos de ancho fijo cuyos valores se alinean a la derecha. Para leer este fichero mediante el controlador CSV de GDAL/OGR es necesario transformarlo en un fichero de valores delimitados. El fichero de salida se denominará `puntos.csv`.  
  
```
root@aemet:/data$ #Damos formato tabular al fichero maestro (campos delimitados por 2 espacios), sustituimos cada par de espacios por una coma y volcamos el resultado en el fichero puntos.csv
root@aemet:/data$ column -t maestro_red_hr_tmin.txt | tr -s [:blank:] "," > puntos.csv

root@aemet:/data$ #Comprobamos las coordenadas que definen el área de extensión del conjunto de puntos
root@aemet:/data$ ogrinfo puntos.csv -oo X_POSSIBLE_NAMES=field_2 -oo Y_POSSIBLE_NAMES=field_3 -oo Z_POSSIBLE_NAMES=field_4 -dialect SQLITE -sql "SELECT ogr_layer_Extent('puntos')"
INFO: Open of `puntos.csv'
      using driver `CSV' successful.

Layer name: SELECT
Geometry: Unknown (any)
Feature Count: 1
Extent: (-9.277000, 35.307000) - (4.269000, 43.740000)
Layer SRS WKT:
(unknown)
Geometry Column = ogr_layer_Extent('puntos')
OGRFeature(SELECT):0
  POLYGON ((-9.277 35.307,4.269 35.307,4.269 43.74,-9.277 43.74,-9.277 35.307))
```

### Cómo crear un fichero CSV con el subconjunto de puntos del área de estudio
El área de estudio (provincia de Alicante) en grados decimales (longitud, latitud) abarca desde (-1.223, 37.779) hasta (0.16, 38.912). Los puntos de la rejilla que se encuentren dentro del área de estudio se extraerán del fichero de entrada `puntos.csv`. El subconjunto de puntos resultante se almacenará en el fichero de salida `puntos_ALC.csv`.  
  
```
root@aemet:/data$ #Extraemos los puntos del área de estudio mediante una selección por rango y los almacenamos en el fichero puntos_ALC.csv
root@aemet:/data$ ogr2ogr -spat -1.223 37.779 0.16 38.912 -f CSV puntos_ALC.csv puntos.csv -oo X_POSSIBLE_NAMES=field_2 -oo Y_POSSIBLE_NAMES=field_3 -oo Z_POSSIBLE_NAMES=field_4 -lco STRING_QUOTING=IF_NEEDED

root@aemet:/data$ #Recuperamos los registros del fichero puntos_ALC.csv sin el encabezado y los almacenamos sobreescribiendo el propio fichero puntos_ALC.csv
root@aemet:/data$ tail -n+2 puntos_ALC.csv > puntos_ALC.tmp && mv puntos_ALC.tmp puntos_ALC.csv
```

### Cómo extraer y volcar en un fichero CSV la serie temporal de temperaturas mínimas correspondiente al área de estudio
Dado que el campo identificador en `puntos_ALC.csv` representa los índices de las columnas correspondientes al área de estudio, se usará este fichero en combinación con el de la serie temporal de toda España para extraer las temperaturas mínimas del área de estudio. La nueva serie temporal reducida se almacenará en el fichero `tmin_ALC_1951_2020.csv` y cada registro representará una estimación individual compuesta de los siguientes campos: `{fecha;identificador_punto;temperatura}`
  
```
root@aemet:/data$ #Extraemos la columna de identificadores del fichero de puntos del área de estudio y la almacenamos en la variable $ids como una lista delimitada por espacios
root@aemet:/data$ ids=$(cut -d, -f1 puntos_ALC.csv | tr '\n' ' ')

root@aemet:/data$ #Usamos awk para extraer la serie de temperaturas mínimas del área de estudio y la almacenamos en el fichero tmin_ALC_1951_2020.csv
root@aemet:/data$ #El comando awk es el intérprete del lenguaje de programación AWK
root@aemet:/data$ #Dado un fichero de texto de entrada, el comando awk ejecuta un conjunto de instrucciones AWK para cada línea del fichero y produce un texto de salida
root@aemet:/data$ #AWK es un lenguaje adecuado para procesar de modo eficiente millones de líneas de texto organizadas en miles de campos delimitados por espacios
root@aemet:/data$ #En nuestro caso, el código AWK lee la primera línea del fichero de entrada y extrae los valores de temperatura mínima correspondientes al área de estudio
root@aemet:/data$ #Este proceso se repite línea a línea hasta el fin de fichero
root@aemet:/data$ #Las tareas concretas que se ejecutan durante el procesamiento de una línea del fichero de entrada son las siguientes:
root@aemet:/data$ #1. Se invoca la función split para convertir la lista de identificadores de punto en un array sobre el que iteramos usando la instrucción de bucle for
root@aemet:/data$ #2. En cada iteración se genera un registro con la fecha (campo 1), el identificador de punto y el valor de temperatura en ese punto
root@aemet:/data$ #   Se debe tener en cuenta que los valores de temperatura se almacenan a partir del campo 2
root@aemet:/data$ #   Por lo tanto, si el identificador de punto es n, el valor de temperatura se extraerá del campo n+1
root@aemet:/data$ time awk -v cols="$ids" 'BEGIN{split(cols, arr_cols, " ")} {for (i in arr_cols) printf "%s;%s;%s\n", $1, arr_cols[i], $(arr_cols[i] + 1)}' tmin_red_SPAIN_1951-2020.geo > tmin_ALC_1951_2020.csv

real	0m12.814s
user	0m12.402s
sys	0m0.412s

root@aemet:/data$ #Comparamos el tamaño de ambas series temporales
root@aemet:/data$ ls -lh tmin*
-rw-r--r-- 1 root root  147M Dec 16 12:34 tmin_ALC_1951_2020.csv
-rw-r--r-- 1 3405 50003 3.1G Jul  2 10:32 tmin_red_SPAIN_1951-2020.geo
```

### Cómo iniciar PostgreSQL
Para realizar operaciones de álgebra relacional con la serie temporal de temperaturas usaremos PostgreSQL. Al tratarse de un servidor de base de datos, es necesario iniciar una instancia del mismo y crear una base de datos transitoria sobre la que operar, tal como se muestra en la siguiente secuencia de instrucciones:  
  
```
root@aemet:/data$ #Editamos el fichero de configuración de permisos de acceso para
root@aemet:/data$ #permitir conexiones locales con la cuenta de usuario postgres
root@aemet:/data$ sed -i '0,/peer$/s//trust/' /etc/postgresql/13/main/pg_hba.conf

root@aemet:/data$ #Creamos un fichero de nombres de servicio (pg_service.conf)
root@aemet:/data$ #y definimos el servicio aemet. Un servicio es simplemente un
root@aemet:/data$ #conjunto de parámetros que conforman una cadena de conexión
root@aemet:/data$ #determinada. En nuestro caso usaremos el servicio aemet para
root@aemet:/data$ #conectarnos con el usuario postgres a una base de datos
root@aemet:/data$ #denominada aemet.
root@aemet:/data$ printf "%s\n%s\n%s\n" [aemet] user=postgres dbname=aemet > /etc/postgresql-common/pg_service.conf

root@aemet:/data$ #Asignamos el servicio aemet a la variable de entorno PGSERVICE.
root@aemet:/data$ #El cliente psql usará por defecto los parámetros de conexión del
root@aemet:/data$ #servicio aemet si no especificamos otros. Es una solución ergonómica
root@aemet:/data$ #para emitir instrucciones SQL desde el shell sin necesidad de
root@aemet:/data$ #indicar los parámetros de conexión en cada invocación. 
root@aemet:/data$ export PGSERVICE=aemet

root@aemet:/data$ #Iniciamos la instancia de PostgreSQL que se generó por defecto
root@aemet:/data$ #durante la instalación del paquete postgresql en Debian
root@aemet:/data$ pg_ctlcluster 13 main start

root@aemet:/data$ #Creamos la base de datos aemet, es decir, la base de datos
root@aemet:/data$ #transitoria sobre la que vamos a operar
root@aemet:/data$ psql -d postgres -c "CREATE DATABASE aemet;"
```

### Cómo importar la serie temporal en la base de datos
Realizaremos una operación de copia manual desde el servidor PostgreSQL para escribir los registros del fichero `tmin_ALC_1951_2020.csv` en una tabla de la nueva base de datos `aemet`.  
  
```
root@aemet:/data$ #Averiguamos la ubicación del directorio de datos del servidor
root@aemet:/data$ #PostgreSQL mediante la instrucción "SHOW data_directory;"
root@aemet:/data$ psql -c "SHOW data_directory;"
       data_directory        
-----------------------------
 /var/lib/postgresql/13/main
(1 row)

root@aemet:/data$ #Para que PostgreSQL tenga acceso al fichero CSV es necesario
root@aemet:/data$ #designar al usuario postgres como propietario del mismo
root@aemet:/data$ #y colocarlo seguidamente en el directorio de datos del
root@aemet:/data$ #servidor PostgreSQL. Para abreviar, cambiamos el nombre
root@aemet:/data$ #del fichero CSV a data.csv
root@aemet:/data$ chown postgres:postgres tmin_ALC_1951_2020.csv
root@aemet:/data$ mv tmin_ALC_1951_2020.csv /var/lib/postgresql/13/main/data.csv

root@aemet:/data$ #Creamos una tabla, denominada aemet, en la que volcar los datos del fichero
root@aemet:/data$ #data.csv. Para reducir el tiempo de escritura, puenteamos el log de datos de
root@aemet:/data$ #PostgreSQL (Write-Ahead Logging o WAL) mediante la instrucción UNLOGGED
root@aemet:/data$ psql -c "CREATE UNLOGGED TABLE aemet (t integer, pid integer, celsius float);"

root@aemet:/data$ #Volcamos los registros del fichero data.csv en la tabla aemet mediante el
root@aemet:/data$ #comando SQL COPY
root@aemet:/data$ time psql -c "COPY aemet FROM 'data.csv' WITH (FORMAT csv, DELIMITER ';');"
COPY 7849376

real	0m4.160s
user	0m0.062s
sys	0m0.013s
```

### Cómo generar ficheros CSV con las series diarias de cada punto de la rejilla listadas por año
La finalidad es automatizar la creación de un fichero CSV de valores de temperatura mínima para cada punto de la rejilla de 0.05 grados. Cada fichero estará compuesto por 365 filas (una por cada día del año) y 70 columnas (una por cada año de la serie 1951 a 2020). Dado que la columna `t` representa fechas como números enteros, las operaciones de extracción del año, mes o día se basan en la división entera.  
  
```
root@aemet:/data$ #Descartamos las estimaciones referidas al 29 de febrero de modo que, dado
root@aemet:/data$ #un año y un punto determinados, el número de estimaciones sea siempre 365
root@aemet:/data$ time psql -c "DELETE FROM aemet WHERE (t/100 % 100)= 2 and (t % 100)=29;"
DELETE 5526

real	0m0.579s
user	0m0.028s
sys	0m0.011s

root@aemet:/data$ #Indexamos de forma solidaria la expresión de extracción del año y el campo
root@aemet:/data$ #del identificador de punto para acelerar las consultas.
root@aemet:/data$ time psql -c "CREATE INDEX ypid_idx ON aemet ((t/10000), pid);"
CREATE INDEX

real	0m3.275s
user	0m0.035s
sys	0m0.015s

root@aemet:/data$ #Obtenemos la lista de identificadores de punto en los que existen estimaciones
root@aemet:/data$ #de temperatura mínima y la volcamos en un fichero denominado pids.
root@aemet:/data$ time psql -t -c "SELECT DISTINCT pid FROM aemet;" | head -n -1 > pids

real	0m1.191s
user	0m0.038s
sys	0m0.005s

root@aemet:/data$ #Creamos un directorio de trabajo, denominado series, en el que almacenar tanto
root@aemet:/data$ #los ficheros intermedios como los ficheros CSV finales
root@aemet:/data$ mkdir -p series

root@aemet:/data$ #Generamos un fichero CSV por cada punto de la rejilla. El algoritmo consiste en
root@aemet:/data$ #iterar sobre la lista de identificadores de punto (fichero pids). Dado un
root@aemet:/data$ #identificador, iteramos a su vez sobre el intervalo de la serie temporal (1951 a 2020).
root@aemet:/data$ #Dado un identificador y año, obtenemos la lista de 365 estimaciones y la almacenamos
root@aemet:/data$ #en un fichero temporal. Una vez obtenidos los 70 ficheros temporales, los
root@aemet:/data$ #concatenamos mediante la instrucción paste, escribimos la salida en un fichero
root@aemet:/data$ #CSV cuyo nombre sigue el patrón [identificador_de_punto].csv y borramos los temporales
root@aemet:/data$ time while read pid; do for y in {1951..2020}; do psql -t -c "\copy (SELECT celsius y_${y} FROM aemet where (t/10000)=${y} and pid=${pid}) to 'series/${y}_${pid}.tmp' csv header"; done; paste -d ";" series/*.tmp > series/"${pid}".csv; rm -rf series/*.tmp; done < pids

real	13m37.614s
user	10m31.713s
sys	1m50.257s
```

### Cómo cerrar la sesión de trabajo en el contenedor aemet
Para terminar la sesión de trabajo en el contenedor aemet y regresar a la línea de comandos del sistema anfitrión, basta con invocar el comando `exit`. Dado que se trata de un contenedor efímero, esto provoca su detención y posterior eliminación.

```
root@aemet:/data$ #Salimos de la línea de comandos del contenedor aemet, lo que provoca su detención y eliminación
root@aemet:/data$ #Los fichero resultantes quedan almacenados en el sistema anfitrión. Por un lado, los ficheros CSV
root@aemet:/data$ #de salida con las series temporales de cada punto se encuentran en el directorio
root@aemet:/data$ #$HOME/gitrepos/etl/tmin/AEMET/data/series. Por su parte, el fichero con el índice de puntos del
root@aemet:/data$ #área de estudio se encuentra en $HOME/gitrepos/etl/tmin/AEMET/data/puntos_ALC.csv
root@aemet:/data$ exit
```


## Serie temporal de temperatura mínima STEAD del CSIC

### Descripción de los datos
[STEAD](http://hdl.handle.net/10261/188989), proporcionado por el Centro Superior de Investigaciones Científicas, es un conjunto de datos de estimaciones diarias de temperatura mínima y máxima sobre una rejilla de 5 Km de resolución. La serie de temperatura mínima del período 1901-2014 para la Península está disponible en un único fichero NetCDF.
El fichero `STEAD/data/DOWNLOAD` contiene la URL de la página de descargas del CSIC y la URL del fichero NetCDF. 

### Cómo ejecutar un contenedor para procesar la serie STEAD
Suponiendo que los datos STEAD se almacenan en `$HOME/gitrepos/etl/tmin/STEAD/data`, se debe ejecutar la siguiente secuencia de instrucciones:

```
cd $HOME/gitrepos/etl/tmin/STEAD/data
docker run -it --rm --name stead -e PROMPT=stead -v $(pwd):/data quommit/tmin
```  
  
Al igual que en el caso práctico anterior, se abre una nueva línea de comandos, pero esta vez con el *prompt* `root@stead:/data$`. El primer paso es comprobar que tenemos acceso al fichero NetCDF previamente descargado:

```
root@stead:/data$ #Listamos el contenido del directorio /data para verificar que
root@stead:/data$ #existe el fichero NetCDF de temperaturas mínimas (tmin*.nc)
root@stead:/data$ ls -lh
total 1.9G
-rw-r--r-- 1 1000 1000  319 Dec 20 12:20 DOWNLOAD
-rw-r--r-- 1 1000 1000 1.9G Dec 15 10:22 tmin_pen.nc
```  
  
Es importante recordar los puntos clave ya explicados:  
  
- A partir de la imagen `quommit/tmin:latest`, se ha iniciado un contenedor nuevo y de un solo uso, identificado por el nombre `stead`.
- Las instrucciones que se escriben tras el *prompt* `root@stead:/data$` se ejecutan en este contenedor como usuario `root` del mismo.
- El directorio de trabajo al iniciar la sesión en la línea de comandos es `/data`. Una vez finalizada la sesión de trabajo y desechado el contenedor mediante el comando `exit`, los ficheros de salida que se hayan almacenado en `/data` estarán disponibles en el directorio `$HOME/gitrepos/etl/tmin/STEAD/data` del sistema de ficheros del anfitrión.

### Cómo crear un fichero NetCDF con el subconjunto de puntos del área de estudio en formato ASCII
Teniendo en cuenta que el área de estudio (provincia de Alicante) va desde -1.223 hasta 0.16 grados de longitud, y desde 37.779 hasta 38.912 grados de latitud, se puede extraer la serie de temperaturas mínimas del área de estudio mediante el comando `ncks` del paquete NCO (NetCDF Operators). Se trata de un comando diseñado para tareas básicas de extracción y transformación de datos NetCDF. La nueva serie reducida se almacenará en el fichero `tmin_ALC_1901_2014.trd`. La secuencia de instrucciones a ejecutar es la siguiente:  
  
```
root@stead:/data$ #Listamos los metadatos del fichero NetCDF para obtener el nombre
root@stead:/data$ #de la variable que almacena los valores de temperatura mínima
root@stead:/data$ ncks -m tmin_pen.nc 
netcdf tmin_pen {
  dimensions:
    Time = UNLIMITED ; // (41638 currently)
    lat = 190 ;
    lon = 230 ;

  variables:
    int Time(Time) ;
      Time:units = "days since 1901-01-01" ;
      Time:long_name = "Time" ;
      Time:axis = "T" ;

    double lat(lat) ;
      lat:units = "degrees_north" ;
      lat:long_name = "lat" ;
      lat:axis = "Y" ;

    double lon(lon) ;
      lon:units = "degrees_east" ;
      lon:long_name = "lon" ;
      lon:axis = "X" ;

    float tn(Time,lat,lon) ;
      tn:units = "Celsius degrees" ;
      tn:_FillValue = 1.e+32f ;
      tn:long_name = "Minimum temperature" ;
} // group /

root@stead:/data$ #Extraemos la serie correspondiente al área de estudio mediante una selección
root@stead:/data$ #por rango y la almacenamos en un nuevo fichero NetCDF denominado tmin_ALC_1901_2014.nc
root@stead:/data$ time ncks -d lon,-1.223,0.16 -d lat,37.779,38.912 tmin_pen.nc tmin_ALC_1901_2014.nc

real	0m18.609s
user	0m18.087s
sys	0m0.521s

root@stead:/data$ #Convertimos el fichero binario NetCDF tmin_ALC_1901_2014.nc en 
root@stead:/data$ #un fichero de texto NetCDF denominado tmin_ALC_1901_2014.trd.
root@stead:/data$ #Es importante indicar tn como variable objetivo
root@stead:/data$ time ncks --trd -C -v tn tmin_ALC_1901_2014.nc > tmin_ALC_1901_2014.trd

real	1m7.492s
user	1m3.833s
sys	0m1.652s
```

### Cómo crear un fichero CSV de estimaciones individuales de temperatura mínima en el área de estudio
El fichero de texto NetCDF `tmin_ALC_1901_2014.trd` consta de 25982112 registros precedidos por una cabecera de 10 líneas. Cada registro representa una estimación individual de temperatura mínima cuya estructura es `Time[i]={t} lat[j]={latitud} lon[k]={longitud} tn[n]={temperatura} Celsius degrees`, donde `t` es el ordinal que abarca desde el día 1 al 41638, es decir, desde el primer al último día de la serie temporal. Los registros se ordenan primero de modo ascendente por día, luego de modo descendente por latitud y por último de modo ascendente por longitud. Esto significa que, para un día concreto, hay 624 estimaciones de temperatura mínima en registros adyacentes, ordenados a su vez por filas (latitud) y columnas (longitud). Por convención se emplea el carácter '_' para simbolizar el valor nulo en aquellos puntos de la rejilla donde no existe estimación de temperatura. Una vez extraídos del fichero NetCDF, los datos se almacenarán en el fichero `tmin_ALC_1901_2014.csv`. Dicho fichero constará igualmente de 25982112 registros, pero en formato CSV. En este sentido, cada registro representará una estimación individual compuesta de los campos `{t;latitud;longitud;temperatura}`. El orden de los registros del fichero CSV será idéntico al del fichero de texto NetCDF. Todo el proceso se resuelve en una única instrucción compuesta de 5 instrucciones simples enlazadas por el operador `|`. Este operador recoge la salida de la instrucción a su izquierda y la pasa como entrada a la instrucción que queda a su derecha. Esto permite construir secuencias de proceso complejas en una sola línea de código tal y como se muestra a continuación:  
  
```
root@stead:/data$ #Lo que sigue es una instrucción compuesta de 5 instrucciones simples.
root@stead:/data$ #Estas instrucciones se encadenan mediante el operador `|`, que toma
root@stead:/data$ #la salida de la primera instrucción y la pasa como entrada a la segunda
root@stead:/data$ #instrucción, y así sucesivamente. A continuación se explica con detalle
root@stead:/data$ #cada una de las instrucciones simples 
root@stead:/data$ #Instrucción 1: tail -n +11 tmin_ALC_1901_2014.trd
root@stead:/data$ #  Eliminamos las 10 líneas de cabecera del fichero NetCDF
root@stead:/data$ #Instrucción 2: sed "s/=/ /g"
root@stead:/data$ #  Sustituimos todas las ocurrencias del caracter "=" por " "
root@stead:/data$ #  de modo que cada registro quede conformado por 10 campos donde
root@stead:/data$ #  los campos 2, 4, 6 y 8 corresponden a los valores de t, latitud,
root@stead:/data$ #  longitud y temperatura respectivamente
root@stead:/data$ #Instrucción 3: cut -d " " -f 2,4,6,8
root@stead:/data$ #  Extraemos el valor t, latitud, longitud y temperatura de cada registro
root@stead:/data$ #Instrucción 4: sed "s/ /;/g"
root@stead:/data$ #  Sustituimos todas las ocurrencias del caracter " " por ";"
root@stead:/data$ #  para formar un CSV de 4 campos delimitados con punto y coma
root@stead:/data$ #Instrucción 5: head -n -1
root@stead:/data$ #  Eliminamos la línea vacía al final del conjunto de registros
root@stead:/data$ time tail -n +11 tmin_ALC_1901_2014.trd | sed "s/=/ /g" | cut -d " " -f 2,4,6,8 | sed "s/ /;/g" | head -n -1 > tmin_ALC_1901_2014.csv

real	0m22.891s
user	0m49.266s
sys	0m6.838s
```

### Cómo iniciar PostgreSQL
Para realizar operaciones de álgebra relacional con la serie temporal de temperaturas usaremos PostgreSQL. Al tratarse de un servidor de base de datos, es necesario iniciar una instancia del mismo y crear una base de datos transitoria sobre la que operar, tal como se muestra en la siguiente secuencia de instrucciones:  
  
```
root@stead:/data$ #Editamos el fichero de configuración de permisos de acceso para
root@stead:/data$ #permitir conexiones locales con la cuenta de usuario postgres
root@stead:/data$ sed -i '0,/peer$/s//trust/' /etc/postgresql/13/main/pg_hba.conf

root@stead:/data$ #Creamos un fichero de nombres de servicio (pg_service.conf)
root@stead:/data$ #y definimos el servicio stead. Un servicio es simplemente un
root@stead:/data$ #conjunto de parámetros que conforman una cadena de conexión
root@stead:/data$ #determinada. En nuestro caso usaremos el servicio stead para
root@stead:/data$ #conectarnos con el usuario postgres a una base de datos
root@stead:/data$ #denominada stead.
root@stead:/data$ printf "%s\n%s\n%s\n" [stead] user=postgres dbname=stead > /etc/postgresql-common/pg_service.conf

root@stead:/data$ #Asignamos el servicio stead a la variable de entorno PGSERVICE.
root@stead:/data$ #El cliente psql usará por defecto los parámetros de conexión del
root@stead:/data$ #servicio stead si no especificamos otros. Es una solución ergonómica
root@stead:/data$ #para emitir instrucciones SQL desde el shell sin necesidad de
root@stead:/data$ #indicar los parámetros de conexión en cada invocación. 
root@stead:/data$ export PGSERVICE=stead

root@stead:/data$ #Iniciamos la instancia de PostgreSQL que se generó por defecto
root@stead:/data$ #durante la instalación del paquete postgresql en Debian
root@stead:/data$ pg_ctlcluster 13 main start

root@stead:/data$ #Creamos la base de datos stead, es decir, la base de datos
root@stead:/data$ #transitoria sobre la que vamos a operar
root@stead:/data$ psql -d postgres -c "CREATE DATABASE stead;"
```

### Cómo importar la serie temporal en la base de datos
Para leer desde PostgreSQL el fichero CSV con la serie temporal del área de estudio usaremos el [módulo file_fdw](https://www.postgresql.org/docs/13/file-fdw.html). Se trata de un [foreign-data wrapper](https://www.postgresql.org/docs/13/ddl-foreign-data.html) que proporciona acceso de lectura a datos almacenados en el sistema de ficheros local.  
  
```
root@stead:/data$ #Averiguamos la ubicación del directorio de datos del servidor
root@stead:/data$ #PostgreSQL mediante la instrucción "SHOW data_directory;"
root@stead:/data$ psql -c "SHOW data_directory;"
       data_directory        
-----------------------------
 /var/lib/postgresql/13/main
(1 row)

root@stead:/data$ #Para que PostgreSQL tenga acceso al fichero CSV es necesario
root@stead:/data$ #designar al usuario postgres como propietario del mismo
root@stead:/data$ #y colocarlo seguidamente en el directorio de datos del
root@stead:/data$ #servidor PostgreSQL. Para abreviar, cambiamos el nombre
root@stead:/data$ #del fichero CSV a data.csv
root@stead:/data$ chown postgres:postgres tmin_ALC_1901_2014.csv
root@stead:/data$ mv tmin_ALC_1901_2014.csv /var/lib/postgresql/13/main/data.csv

root@stead:/data$ #Añadimos la extensión file_fdw en la base de datos stead.
root@stead:/data$ psql -c "CREATE EXTENSION file_fdw;"

root@stead:/data$ #Configuramos un punto de acceso al sistema de ficheros y
root@stead:/data$ #definimos la tabla foránea mediante la que se leerán los
root@stead:/data$ #datos del fichero data.csv
root@stead:/data$ psql -c "CREATE SERVER filesys FOREIGN DATA WRAPPER file_fdw;"
root@stead:/data$ psql -c "CREATE FOREIGN TABLE csv (t integer, lat float, lon float, celsius float) SERVER filesys OPTIONS (filename 'data.csv', format 'csv', header 'FALSE', delimiter ';', null '_');"
```

### Cómo generar ficheros CSV con las series diarias de cada punto de la rejilla listadas por año
La finalidad es automatizar la creación de un fichero CSV de valores de temperatura mínima para cada punto de la rejilla de 5 km. Cada fichero estará compuesto por 365 filas (una por cada día del año) y 114 columnas (una por cada año de la serie 1901 a 2014).  
  
```
root@stead:/data$ #Creamos una tabla, denominada stead, en la que volcar los datos de la tabla
root@stead:/data$ #foránea enlazada con el fichero data.csv. De la tabla stead extraeremos cada
root@stead:/data$ #serie diaria por año y punto de la rejilla. Su estructura difiere de la original
root@stead:/data$ #para facilitar consultas rápidas por año y por punto de la rejilla. El primer
root@stead:/data$ #campo hace referencia a la fecha de estimación y el segundo es un identificador
root@stead:/data$ #numérico del punto de la rejilla. El último campo almacena el valor estimado de
root@stead:/data$ #temperatura mínima. Es importante puentear el log de datos de PostgreSQL (WAL
root@stead:/data$ #por las siglas de Write-Ahead Logging) para reducir el tiempo de escritura.
root@stead:/data$ #Ese es el motivo por el que empleamos la instrucción UNLOGGED
root@stead:/data$ psql -c "CREATE UNLOGGED TABLE stead (d date, pid integer, celsius float);"

root@stead:/data$ #Efectuamos el volcado en la tabla stead. La asignación de los identificadores
root@stead:/data$ #numéricos de punto se hace siguiendo el criterio de ordenación del fichero
root@stead:/data$ #NetCDF original, a saber, latitud descendente y longitud ascendente.
root@stead:/data$ time psql -c "INSERT INTO stead SELECT (date '1900-12-31' + t), row_number() over (partition by t order by t, lat desc, lon), celsius FROM csv;"
INSERT 0 25982112

real	0m49.163s
user	0m0.024s
sys	0m0.015s

root@stead:/data$ #Descartamos las estimaciones referidas al 29 de febrero de modo que, dado
root@stead:/data$ #un año y un punto determinados, el número de estimaciones sea siempre 365
root@stead:/data$ psql -c "DELETE FROM stead WHERE extract(day from d)=29 and extract(month from d)=2;"
DELETE 17472

root@stead:/data$ #Indexamos de forma solidaria la expresión de extracción del año y el
root@stead:/data$ #campo del identificador de punto. Esto permite acelerar las consultas.
root@stead:/data$ time psql -c "CREATE INDEX ypid_idx ON stead (extract(year from d), pid);"

real	0m12.991s
user	0m0.038s
sys	0m0.014s

root@stead:/data$ #Obtenemos la lista de identificadores de punto en los que existen estimaciones
root@stead:/data$ #de temperatura mínima y la volcamos en un fichero denominado pids.
root@stead:/data$ time psql -t -c "SELECT DISTINCT pid FROM stead WHERE celsius IS NOT NULL;" | head -n -1 > pids

real	0m3.566s
user	0m0.034s
sys	0m0.007s

root@stead:/data$ #Creamos un directorio de trabajo, denominado series, en el que almacenar tanto
root@stead:/data$ #los ficheros intermedios como los ficheros CSV finales
root@stead:/data$ mkdir -p series

root@stead:/data$ #Generamos un fichero CSV por cada punto de la rejilla. El algoritmo consiste en
root@stead:/data$ #iterar sobre la lista de identificadores de punto (fichero pids). Dado un
root@stead:/data$ #identificador, iteramos a su vez sobre el intervalo de la serie temporal (1901 a 2014).
root@stead:/data$ #Dado un identificador y año, obtenemos la lista de 365 estimaciones y la almacenamos
root@stead:/data$ #en un fichero temporal. Una vez obtenidos los 114 ficheros temporales, los
root@stead:/data$ #concatenamos mediante la instrucción paste, escribimos la salida en un fichero
root@stead:/data$ #CSV cuyo nombre sigue el patrón [identificador_de_punto].csv y borramos los temporales
root@stead:/data$ time while read pid; do for y in {1901..2014}; do psql -t -c "\copy (SELECT celsius y_${y} FROM stead where extract(year from d)=${y} and pid=${pid}) to 'series/${y}_${pid}.tmp' csv header"; done; paste -d ";" series/*.tmp > series/"${pid}".csv; rm -rf series/*.tmp; done < pids

real	28m37.238s
user	22m25.192s
sys	3m44.280s
```

### Cómo generar un fichero CSV con el índice de puntos del área de estudio
Los ficheros CSV con las series diarias de temperatura no almacenan la localización del punto al que hacen referencia. Como medio que facilite la selección espacial, es conveniente confeccionar la relación de puntos que conforman la rejilla de 5 km en el área de estudio. El fichero CSV resultante se denominará `puntos_ALC.csv`. Cada registro del fichero CSV representa un punto que consta de los campos `{pid;latitud;longitud}`, donde `pid` es un entero secuencial que sirve para identificar el punto. El criterio de ordenación de la secuencia es, primero, por latitud descendente y, a continuación, por longitud ascendente.

```
root@stead:/data$ #Una solución simple para generar el índice de puntos de la rejilla es emplear
root@stead:/data$ #la tabla foránea csv y seleccionar únicamente las estimaciones del primer día
root@stead:/data$ #de la serie, ordenadas por latitud descendente y longitud ascendente.
root@stead:/data$ time psql -c "\copy (SELECT row_number() over() pid, lat, lon FROM csv WHERE t=1 ORDER BY lat desc, lon) to 'puntos_ALC.csv' csv header delimiter ';'"
COPY 624

real	0m16.853s
user	0m0.027s
sys	0m0.014s
```

### Cómo cerrar la sesión de trabajo en el contenedor stead
Para terminar la sesión de trabajo en el contenedor stead y regresar a la línea de comandos del sistema anfitrión, basta con invocar el comando `exit`. Dado que se trata de un contenedor efímero, esto provoca su detención y posterior eliminación.

```
root@stead:/data$ #Salimos de la línea de comandos del contenedor stead, lo que provoca su detención y eliminación
root@stead:/data$ #Los fichero resultantes quedan almacenados en el sistema anfitrión. Por un lado, los ficheros CSV
root@stead:/data$ #de salida con las series temporales de cada punto se encuentran en el directorio
root@stead:/data$ #$HOME/gitrepos/etl/tmin/STEAD/data/series. Por su parte, el fichero con el índice de puntos del
root@stead:/data$ #área de estudio se encuentra en $HOME/gitrepos/etl/tmin/STEAD/data/puntos_ALC.csv
root@stead:/data$ exit
```
