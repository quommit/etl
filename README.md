# Casos prácticos de extracción, transformación y carga de datos
Este repositorio reúne una colección de casos prácticos de tratamiento de datos geográficos procedentes preferentemente de fuentes oficiales.

### ¿Cuál es la finalidad de este repositorio?
La finalidad última es crear y mantener documentación técnica detallada de determinados casos de uso de datos geográficos de naturaleza diversa. El propósito fundamental de esta documentación es la reproducibilidad, de modo que todos los pasos puedan replicarse aunque varíe la extensión del área de estudio. La documentación está diseñada como instrumento para sesiones de aprendizaje así como para tareas de investigación.

### Cómo usar este repositorio
El repositorio se organiza en un árbol de directorios. Cada directorio de nivel superior constituye un caso práctico, y contiene un fichero `README` con la documentación necesaria para su reproducción.
